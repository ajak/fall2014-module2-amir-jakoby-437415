<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
    		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Calculator</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    	<!--[if lt IE 9]>
	           <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
   		   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    	<![endif]-->
		<link rel="stylesheet" type="text/css" href="calculator.css">
	</head>
	<body>
		<?php

		function calculate($first, $second, $operator) {
			if (isset($_GET['first']) and isset($_GET['second']) and isset($_GET['operator'])) {
			
	#			echo "is numeric!!!!: ".empty($second);
         #               if (!empty($first) && !empty($second) && !empty($operator)) {
                                #lets check if there are numbers
                                if (is_numeric($first) && is_numeric($second)) {
                                #now lets calculate!
                                $num1 = floatval($first);
                                $num2 = floatval($second);

                                switch ($operator) {
                                        case "addition":
                                                return $num1 + $num2;
                                        break;
                                        case "subtraction":
                                                return $num1 - $num2;
                                        break;
                                        case "multiplication":
                                                return $num1 * $num2;
                                        break;
                                        case "division":
						if ($num2 == 0) {
							return '';
						}
                                                return $num1 / $num2;
                                        break;
                                }
                                }

                                
          #              }
                	}
			return '';
		};

                if (isset($_GET['first']) and isset($_GET['second']) and isset($_GET['operator'])) {
                $first = $_GET['first'];
                $second = $_GET['second'];
                $operator = $_GET['operator'];
		} else {
		$first = '';
                $second = '';
                $operator = '';
		}
		?>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	<div class="container">
      <div class="header">
        <h2 class="text-muted">Calculator</h2>
      </div>
	<div class="row marketing">
		<div class="col-lg-6">
			<form role="form" method="GET">
				<div class="form-group">
					<label for="first">First</label>
					<input type="text" id="first" class="form-control" name="first" value="<?php echo htmlentities($first) ?>">
				</div>
				
				<div class="form-group">
					<label for="second">Second</label>
					<input type="text" id="second" class="form-control" name="second" value="<?php echo htmlentities($second) ?>">
				</div>
				<div class="col-md-offset-1">
				<label class="radio">
				  <input type="radio" name="operator" id="inlineRadio1" value="addition" <?php echo $operator == "addition"? 'checked' : '' ?>>+
				</label>
				<label class="radio">
				  <input type="radio" name="operator" id="inlineRadio2" value="subtraction" <?php echo $operator == "subtraction"? 'checked' : '' ?>>-
				</label>
				<label class="radio">
				  <input type="radio" name="operator" id="inlineRadio3" value="multiplication" <?php echo $operator == "multiplication"? 'checked' : '' ?>>*
				</label>
                                <label class="radio">
                                  <input type="radio" name="operator" id="inlineRadio4" value="division" <?php echo $operator == "division"? 'checked' : '' ?>>/
                                </label>
				</div>
				<input type="submit" class="btn btn-primary btn-block" value="Calculate!">
			</form>
		</div>
	</div>
	<div class="row marketing">
		<div class="col-lg-6">
			<p class="bg-primary center">
			<?php
				echo htmlentities(calculate($first, $second, $operator));
			?>
			</p>
		</div>
	</div>
	</div>
	</body>
</html>
